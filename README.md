# Async-API-Testing

## Components

* Async-api
  * Main API providing a synchronous interface, and a callback endpoint
  * 2 Endpoints
    * GET /request
    * POST /callback
  * Dockerfile, Kubernetes yaml
* Mock-api
  * Mock API taking a request, and providing an asynchronous callback 0.15 -> 1.5s later
  * Functionality currently simply doubles a provided number, then Thread.sleep() to introduce delay
* Zipkin
  * Basic Kubernetes yaml to provide zipkin within the namespace
* Redis
  * Basic Kubernetes yaml and config

## Roadmap

* Initial Build
  * Completed and tested locally
  * APIs work under load, ~220 RPS max on laptop
  * Zipkin integration works
* Docker setup
  * Dockerfiles setup and tested locally
  * API stored at /api/api.jar
  * Config mounted at /api/config
* Kubernetes Setup
  * Deployment + Service + Ingress setup for each
  * 3 distinct deployments
    * async-api-deployment
    * mock-api-deployment
    * zipkin-deployment
  * Ingress using xip.io
    * async-api.async-api-testing.10.8.0.21.xip.io
    * mock-api.async-api-testing.10.8.0.21.xip.io
    * zipkin.async-api-testing.10.8.0.21.xip.io
  * Load testing
    * RPS 330-350+ stable without latency increase
* Integration of Redis
  * Included redis deployment in namespace
  * Async API callback url is now /callback/app-id/request-id
    * If app-id doesn't match, push to redis queue (app-id, request)
    * Every API subscribes to (app-id)
  * Allows horizontal scalability of async-api
* Load Testing running on Cluster
  * Setup locust-master and locust-slave services
  * locust-master.async-api-testing.10.8.0.21.xip.io
  * 1 Async + 3 Mock
    * Stable at 500 Clients ~370 RPS 
    * Moving to 600 Clients ~380 RPS induces failure
      * Threads set at 400 - likely exhaustion
  * 2 Async + 3 Mock
    * Stable at 500 Clients - ~370 RPS
    * Stable at 600 Clients - ~450 RPS
    * Stable at 700 Clients - ~522 RPS
    * Stable at 800 Clients - ~540-550 RPS
      * Breakdown of improvement, 2x400 = 800 threads?
      * 95% increasing
      * 900 Clients - ~560 RPS
        * 95% -> ~2S
      * 1000 Client - ~ 500 RPS
        * 95% -> 2.6S
  * 3 Async + 3 Mock
    * 600 as before
    * 800 - ~590RPS
    * 1000 - ~650RPS
      * 95% ~ 2.1S
  * 3 Async + 5 Mock
    * 800 - ~580 RPS
    * 1000 - ~600 RPS
    * Long Requests
      * Redis/Callback returned
        * Original API Not returning
        * Possible issue with Redis listener (?)
        * ![Zipkin Slow Request](images/1_zipkin_slow.png)
        * Test setting Exectuor in v3
  * 3 Async + 5 Mock + Redis Executor
    * 600 - ~450RPS
    * 800 - ~600 RPS
    * 1000 - ~750 RPS
    * 1250 - ~930 RPS
    * 1300 - ~960 RPS
    * 1350 - ~990 RPS
    * 1400 - ~ 1025 RPS
      * 95% remains at 1.5s
    * Longer duration test @ 1400
      * Increased response time over time
      * Reduced request rate over time (fixed client number + slower responses)
      * Some kind of memory/thread leak (?)
      * ![Locust Screenshot](images/2_locust_test.png)
      * VMs are also heavily loaded ~75-80%
      * Delays in zipkin similar to before, and callback requests themselves taking time (>1.5s just for callback)
      * Issue Identified
        * Invalid use of Hashmap put(k: k, v: v) and remove(k: v)
        * Improved Hashmap constructor parameters
        * Primary Issues
          * Memory Leak due to invalid removal
        * Improvement
          * Wrap access to structures in functions
            * registerLock(UUID, lock)
            * deRegisterLockAndValue(UUID, lock) -> dataMap value or null 
          * Expose map metrics via Prometheus integration for health
            * LockMap.size() == number of open requests
          * ![Locust Screenshot](images/3_locust_test.png)
* Next Steps
  * Setup build pipeline for API -> Docker images -> Gitlab Registry
  * Setup deploy pipeline for Api/Mock/Zipkin/(MQ or Redis)
  * Add log shipping into ELK
    * Sidecar Filebeat?
  * Prometheus to scrape /actuator/prometheus in all pods
