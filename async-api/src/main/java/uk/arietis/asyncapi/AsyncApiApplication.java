package uk.arietis.asyncapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import uk.arietis.asyncapi.config.ApiConfig;

import java.util.UUID;


@SpringBootApplication
@EnableConfigurationProperties(ApiConfig.class)
public class AsyncApiApplication {

	public static UUID applicationID = UUID.randomUUID();

	public static void main(String[] args) {
		SpringApplication.run(AsyncApiApplication.class, args);
	}

}
