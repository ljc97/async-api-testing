package uk.arietis.asyncapi.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

@ConfigurationProperties(prefix="api")
@Data
public class ApiConfig {
    private URI callbackURI;
    private URI serviceURI;
}
