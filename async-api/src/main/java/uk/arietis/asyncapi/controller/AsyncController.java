package uk.arietis.asyncapi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import uk.arietis.asyncapi.data.CallbackRequest;
import uk.arietis.asyncapi.data.Response;

import java.util.UUID;

public interface AsyncController {

    public Response retrieveData();

    public ResponseEntity<String> handleCallback(CallbackRequest callbackRequest, UUID applicationID, UUID requestID);
}
