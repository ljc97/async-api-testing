package uk.arietis.asyncapi.controller;

import uk.arietis.asyncapi.data.MQCallbackRequest;

public interface MQController {
    public void handleMQCallback(MQCallbackRequest mqCallbackRequest);
}
