package uk.arietis.asyncapi.controller.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uk.arietis.asyncapi.AsyncApiApplication;
import uk.arietis.asyncapi.controller.AsyncController;
import uk.arietis.asyncapi.data.CallbackRequest;
import uk.arietis.asyncapi.data.Response;
import uk.arietis.asyncapi.service.AsyncService;
import uk.arietis.asyncapi.service.MQService;

import java.util.UUID;

@RestController
@Slf4j
@RequiredArgsConstructor
public class AsyncControllerImpl implements AsyncController {

    @NonNull
    private AsyncService asyncService;

    @NonNull
    private MQService mqService;


    @Override
    @GetMapping(value="/request")
    public Response retrieveData() {
        return asyncService.retrieveData();
    }

    @Override
    @PostMapping(value="/callback/{application_id}/{request_id}")
    public ResponseEntity<String> handleCallback(@RequestBody CallbackRequest callbackRequest, @PathVariable(value = "application_id") UUID applicationID, @PathVariable(value = "request_id") UUID requestID) {
        if(applicationID.equals(AsyncApiApplication.applicationID)) {
            return asyncService.handleCallback(callbackRequest, requestID);
        }
        else {
            Boolean status = mqService.forwardMessage(applicationID, requestID, callbackRequest);
            return status ? ResponseEntity.ok().build() : ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
