package uk.arietis.asyncapi.controller.impl;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Service;
import uk.arietis.asyncapi.AsyncApiApplication;
import uk.arietis.asyncapi.controller.MQController;
import uk.arietis.asyncapi.data.CallbackRequest;
import uk.arietis.asyncapi.data.MQCallbackRequest;
import uk.arietis.asyncapi.service.AsyncService;
import uk.arietis.asyncapi.service.MQService;

import java.util.concurrent.*;

@Slf4j
@Service
public class MQControllerImpl implements MQController {

    @NonNull
    private AsyncService asyncService;

    @NonNull
    private MeterRegistry meterRegistry;

    private ThreadPoolExecutor mqTaskExecutor = new ThreadPoolExecutor(4, 30, 60L, TimeUnit.SECONDS, new SynchronousQueue());

    private MQControllerImpl(AsyncService asyncService, MeterRegistry meterRegistry){
        this.asyncService = asyncService;
        this.meterRegistry = meterRegistry;
        meterRegistry.gauge("mq_task_executor.threads", mqTaskExecutor, ThreadPoolExecutor::getActiveCount);
    }

    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setTaskExecutor(mqTaskExecutor);
        MessageListenerAdapter mqCallbackMessageListener = new MessageListenerAdapter(this, "handleMQCallback");
        mqCallbackMessageListener.setSerializer(new GenericJackson2JsonRedisSerializer());
        mqCallbackMessageListener.afterPropertiesSet();
        container.addMessageListener(mqCallbackMessageListener, new PatternTopic(AsyncApiApplication.applicationID.toString()));
        log.info("Registered Listener for Pattern {}", AsyncApiApplication.applicationID);
        return container;
    }

    @Override
    public void handleMQCallback(MQCallbackRequest mqCallbackRequest) {
        log.info("Recieved MQ Callback {}",mqCallbackRequest);
        asyncService.handleCallback((CallbackRequest) mqCallbackRequest, mqCallbackRequest.getRequestID());
    }
}
