package uk.arietis.asyncapi.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AckResponse {
    private UUID ackID;
    private Date dateReceived;
}
