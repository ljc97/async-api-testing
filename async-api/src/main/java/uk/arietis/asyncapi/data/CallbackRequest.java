package uk.arietis.asyncapi.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CallbackRequest  {
    private UUID ackID;
    private UUID responseID;
    private Integer newData;
    private Date dateResponded;
}
