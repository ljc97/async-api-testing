package uk.arietis.asyncapi.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MQCallbackRequest extends CallbackRequest {

    public MQCallbackRequest(CallbackRequest callbackRequest, UUID requestID, UUID sourceApplicationID, UUID destinationApplicationID) {
        super(callbackRequest.getAckID(), callbackRequest.getResponseID(), callbackRequest.getNewData(), callbackRequest.getDateResponded());
        this.requestID = requestID;
        this.sourceApplicationID = sourceApplicationID;
        this.destinationApplicationID = destinationApplicationID;
    }

    private UUID requestID;
    private UUID sourceApplicationID;
    private UUID destinationApplicationID;
}
