package uk.arietis.asyncapi.data;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class Response {
    private UUID requestID;
    private UUID ackID;
    private UUID responseID;
    private Integer myData;
    private Integer newData;
}
