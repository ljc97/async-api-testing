package uk.arietis.asyncapi.gateway;

import uk.arietis.asyncapi.data.AckResponse;
import uk.arietis.asyncapi.data.Request;

import java.net.URI;

public interface AsyncGateway {

    public AckResponse submitRequest(URI uri, Request request);
}
