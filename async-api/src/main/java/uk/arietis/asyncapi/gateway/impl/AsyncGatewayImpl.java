package uk.arietis.asyncapi.gateway.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import uk.arietis.asyncapi.data.AckResponse;
import uk.arietis.asyncapi.data.Request;
import uk.arietis.asyncapi.gateway.AsyncGateway;

import java.net.URI;

@Service
@Slf4j
@RequiredArgsConstructor
public class AsyncGatewayImpl implements AsyncGateway {

    @NonNull
    private RestTemplate restTemplate;

    @Override
    public AckResponse submitRequest(URI uri, Request request) {

        ResponseEntity<AckResponse> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(uri, request, AckResponse.class);
        }
        catch(HttpStatusCodeException exc) {
            HttpStatus httpStatus = exc.getStatusCode();
            log.error("Received Status {} for Request {} to {}", httpStatus, request, uri);
            return null;
        }
        catch(RestClientException exc) {
            log.error("Received RestClientException from Callback", exc);
            return null;
        }
        HttpStatus httpStatus = responseEntity.getStatusCode();
        log.info("Received Status {} for Request {} to {}", httpStatus, request, uri);
        return responseEntity.getBody();
    }
}
