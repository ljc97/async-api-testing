package uk.arietis.asyncapi.service;

import uk.arietis.asyncapi.data.CallbackRequest;

import java.util.UUID;

public interface MQService {
    public Boolean forwardMessage(UUID applicationID, UUID requestID, CallbackRequest callbackRequest);
}
