package uk.arietis.asyncapi.service.impl;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import uk.arietis.asyncapi.AsyncApiApplication;
import uk.arietis.asyncapi.config.ApiConfig;
import uk.arietis.asyncapi.data.AckResponse;
import uk.arietis.asyncapi.data.CallbackRequest;
import uk.arietis.asyncapi.data.Request;
import uk.arietis.asyncapi.data.Response;
import uk.arietis.asyncapi.gateway.AsyncGateway;
import uk.arietis.asyncapi.service.AsyncService;

import java.net.URI;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class AsyncServiceImpl implements AsyncService {

    @NonNull
    private AsyncGateway asyncGateway;

    @NonNull
    private MeterRegistry meterRegistry;

    @NonNull
    private ApiConfig apiConfig;

    private ConcurrentHashMap<UUID, Object> lockMap = new ConcurrentHashMap<>(128, 0.75f, 64);
    private ConcurrentHashMap<Object, CallbackRequest> dataMap = new ConcurrentHashMap<>(128, 0.75f, 64);

    public AsyncServiceImpl(AsyncGateway asyncGateway, MeterRegistry meterRegistry, ApiConfig apiConfig) {
        this.asyncGateway = asyncGateway;
        this.meterRegistry = meterRegistry;
        this.apiConfig = apiConfig;
        meterRegistry.gauge("service_lock_map.size", Tags.empty(),this.lockMap, ConcurrentHashMap::size);
        meterRegistry.gauge("service_data_map.size", Tags.empty(),this.dataMap, ConcurrentHashMap::size);
    }

    // Store Lock against UUID, No sync required for insert
    private void registerLock(UUID requestID, Object lock){
        lockMap.put(requestID, lock);
    }

    // Retrieve Data or Null, removing Lock and removing data (if present)
    private CallbackRequest deRegisterLockAndData(UUID requestID, Object lock) {
        synchronized(lock) {
            lockMap.remove(requestID);
            return dataMap.remove(lock);
        }
    }

    @Override
    public Response retrieveData() {

        UUID requestID = UUID.randomUUID();
        MDC.put("request-id", requestID.toString());
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUri(apiConfig.getCallbackURI()).pathSegment(AsyncApiApplication.applicationID.toString(), requestID.toString());
        URI callbackURI = uriComponentsBuilder.build().toUri();
        Request request = new Request(callbackURI, 2);

        Object lock = new Object();
        registerLock(requestID, lock);

        log.info("Constructed request {}", request);
        AckResponse ackResponse = asyncGateway.submitRequest(apiConfig.getServiceURI(), request);
        log.info("Request submitted, received ack {}", ackResponse);

        if(ackResponse == null) {
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        MDC.put("ack-id", ackResponse.getAckID().toString());

        Response response = new Response();
        response.setAckID(ackResponse.getAckID());
        response.setRequestID(requestID);
        response.setMyData(request.getMyData());

        synchronized (lock) {
            try {
                lock.wait(5000);
            } catch (InterruptedException exc) {
                log.error("InterruptedException", exc);
            }
        }

        CallbackRequest callbackRequest = deRegisterLockAndData(requestID, lock);
        if(callbackRequest != null) {
            MDC.put("callback-id", callbackRequest.getResponseID().toString());
            log.info("Received Callback Data {}", callbackRequest);
            response.setNewData(callbackRequest.getNewData());
            response.setResponseID(callbackRequest.getResponseID());
        }
        else {
            log.warn("No Callback Received for Request {}", requestID);
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.info("Returning response {}",response);
        return response;
    }

    @Override
    public ResponseEntity<String> handleCallback(CallbackRequest callbackRequest, UUID requestID) {
        MDC.put("request-id", requestID.toString());
        MDC.put("ack-id", callbackRequest.getAckID().toString());
        MDC.put("callback-id",callbackRequest.getResponseID().toString());

        log.info("Received Callback {}", callbackRequest);

        Object lock = lockMap.getOrDefault(requestID, null);

        if(lock != null) {
            synchronized (lock) {
                if (lockMap.contains(lock)) {
                    // push data
                    dataMap.put(lock, callbackRequest);
                    lock.notify();
                }
            }
        }

        return ResponseEntity.status(HttpStatus.OK).body(null);
    }
}
