package uk.arietis.asyncapi.service.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import uk.arietis.asyncapi.AsyncApiApplication;
import uk.arietis.asyncapi.data.CallbackRequest;
import uk.arietis.asyncapi.data.MQCallbackRequest;
import uk.arietis.asyncapi.service.MQService;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class MQServiceImpl implements MQService {

    @NonNull
    private RedisTemplate redisTemplate;

    @Override
    public Boolean forwardMessage(UUID applicationID, UUID requestID, CallbackRequest callbackRequest) {
        MQCallbackRequest mqCallbackRequest = new MQCallbackRequest(callbackRequest, requestID, AsyncApiApplication.applicationID, applicationID);

        try {
            log.info("Submitting MQ Callback {}", mqCallbackRequest);
            redisTemplate.convertAndSend(applicationID.toString(), mqCallbackRequest);
            return true;
        } catch (Exception exc) {
            log.error("Error submitting message to MQ", exc);
            return false;
        }
    }
}
