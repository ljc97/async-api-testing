package uk.arietis.asyncmock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsyncMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsyncMockApplication.class, args);
	}

}
