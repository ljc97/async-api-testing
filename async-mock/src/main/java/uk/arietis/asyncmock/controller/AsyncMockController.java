package uk.arietis.asyncmock.controller;

import uk.arietis.asyncmock.data.AckResponse;
import uk.arietis.asyncmock.data.Request;

public interface AsyncMockController {
    public AckResponse processMessage(Request request);
}
