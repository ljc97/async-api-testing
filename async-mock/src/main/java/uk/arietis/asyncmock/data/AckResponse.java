package uk.arietis.asyncmock.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
public class AckResponse {
    private UUID ackID;
    private Date dateReceived;
}
