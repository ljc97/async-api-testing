package uk.arietis.asyncmock.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
public class CallbackRequest {
    private UUID ackID;
    private UUID responseID;
    private Integer newData;
    private Date dateResponded;
}
