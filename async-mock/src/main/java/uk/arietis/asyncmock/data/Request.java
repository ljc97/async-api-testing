package uk.arietis.asyncmock.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URI;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Request {

    private URI callbackURI;
    private Integer myData;

}
