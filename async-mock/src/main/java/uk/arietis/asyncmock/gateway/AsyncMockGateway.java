package uk.arietis.asyncmock.gateway;

import uk.arietis.asyncmock.data.CallbackRequest;

import java.net.URI;

public interface AsyncMockGateway {
    public Boolean submitCallback(URI callbackURL, CallbackRequest callbackRequest);
}
