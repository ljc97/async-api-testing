package uk.arietis.asyncmock.gateway.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import uk.arietis.asyncmock.data.CallbackRequest;
import uk.arietis.asyncmock.gateway.AsyncMockGateway;

import java.net.URI;

@Service
@Slf4j
@RequiredArgsConstructor
public class AsyncGatewayImpl implements AsyncMockGateway {

    @NonNull
    private RestTemplate restTemplate;

    @Override
    public Boolean submitCallback(URI callbackURL, CallbackRequest callbackRequest) {
        ResponseEntity<String> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(callbackURL, callbackRequest, String.class);
        }
        catch(HttpStatusCodeException exc) {
            HttpStatus httpStatus = exc.getStatusCode();
            log.info("Received Status {} for Callback ID {} to {}", httpStatus, callbackRequest.getResponseID(), callbackURL);
            return false;
        }
        catch(RestClientException exc) {
            log.error("Received RestClientException from Callback", exc);
            return false;
        }
        HttpStatus httpStatus = responseEntity.getStatusCode();
        log.info("Received Status {} for Callback ID {} to {}", httpStatus, callbackRequest.getResponseID(), callbackURL);
        return httpStatus.is2xxSuccessful();
    }
}
