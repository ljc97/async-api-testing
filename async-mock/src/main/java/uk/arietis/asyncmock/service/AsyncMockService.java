package uk.arietis.asyncmock.service;

import uk.arietis.asyncmock.data.AckResponse;
import uk.arietis.asyncmock.data.CallbackRequest;
import uk.arietis.asyncmock.data.Request;

import java.net.URI;

public interface AsyncMockService {

    public AckResponse processMessage(Request request);

    public void submitCallbackAsynchronously(URI callbackUri, CallbackRequest callbackRequest);
}
