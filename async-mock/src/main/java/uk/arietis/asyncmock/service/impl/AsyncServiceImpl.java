package uk.arietis.asyncmock.service.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.cloud.sleuth.instrument.async.TraceableExecutorService;
import org.springframework.stereotype.Service;
import uk.arietis.asyncmock.data.AckResponse;
import uk.arietis.asyncmock.data.CallbackRequest;
import uk.arietis.asyncmock.data.Request;
import uk.arietis.asyncmock.gateway.AsyncMockGateway;
import uk.arietis.asyncmock.service.AsyncMockService;

import java.net.URI;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
@RequiredArgsConstructor
public class AsyncServiceImpl implements AsyncMockService {

    @NonNull
    private BeanFactory beanFactory;

    @NonNull
    private AsyncMockGateway asyncGateway;

    private ExecutorService ex = Executors.newCachedThreadPool();

    @Override
    public AckResponse processMessage(Request request) {
        log.info("Received request {}", request);
        AckResponse ackResponse = new AckResponse(UUID.randomUUID(), new Date());
        CallbackRequest callbackRequest = new CallbackRequest(ackResponse.getAckID(), UUID.randomUUID(), request.getMyData() * 2,new Date());
        submitCallbackAsynchronously(request.getCallbackURI(), callbackRequest);
        log.info("Returned response {}", ackResponse);
        return ackResponse;
    }

    @Override
    public void submitCallbackAsynchronously(URI callbackUri, CallbackRequest callbackRequest) {
        CompletableFuture<Boolean> callbackStatus = CompletableFuture.supplyAsync(() -> {

            // Introduce Random Delay
            Random random = new Random();
            Integer delay = 150 + random.nextInt(1350);
            log.info("Delaying for {} ms", delay);

            try {
                Thread.sleep(delay.longValue());
            } catch (InterruptedException exc){
                log.error("Unable to sleep callback thread",exc);
            }


            // Submit Callback
            log.info("Submitted Callback {}", callbackRequest);
            return asyncGateway.submitCallback(callbackUri, callbackRequest);
        }, new TraceableExecutorService(beanFactory,ex));
    }
}
