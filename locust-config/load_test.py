from locust import HttpUser, task, between

class TestUser(HttpUser):
    wait_time = between(0,1)

    @task(1)
    def index(self):
        self.client.get("/request")